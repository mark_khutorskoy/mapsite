USE [master]
GO
/****** Object:  Database [dbFilmMap]    Script Date: 22.04.2019 21:41:12 ******/
CREATE DATABASE [dbFilmMap]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'dbFilmMap', FILENAME = N'D:\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\dbFilmMap.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'dbFilmMap_log', FILENAME = N'D:\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\dbFilmMap_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [dbFilmMap] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [dbFilmMap].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [dbFilmMap] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [dbFilmMap] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [dbFilmMap] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [dbFilmMap] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [dbFilmMap] SET ARITHABORT OFF 
GO
ALTER DATABASE [dbFilmMap] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [dbFilmMap] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [dbFilmMap] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [dbFilmMap] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [dbFilmMap] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [dbFilmMap] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [dbFilmMap] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [dbFilmMap] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [dbFilmMap] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [dbFilmMap] SET  DISABLE_BROKER 
GO
ALTER DATABASE [dbFilmMap] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [dbFilmMap] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [dbFilmMap] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [dbFilmMap] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [dbFilmMap] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [dbFilmMap] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [dbFilmMap] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [dbFilmMap] SET RECOVERY FULL 
GO
ALTER DATABASE [dbFilmMap] SET  MULTI_USER 
GO
ALTER DATABASE [dbFilmMap] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [dbFilmMap] SET DB_CHAINING OFF 
GO
ALTER DATABASE [dbFilmMap] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [dbFilmMap] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [dbFilmMap] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'dbFilmMap', N'ON'
GO
USE [dbFilmMap]
GO
/****** Object:  Table [dbo].[ColorScheme]    Script Date: 22.04.2019 21:41:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ColorScheme](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[background] [nvarchar](10) NOT NULL,
	[maincolor] [nvarchar](10) NOT NULL,
	[additional] [nvarchar](10) NOT NULL,
	[mapAdditional] [nvarchar](10) NOT NULL,
	[mapActive] [nchar](10) NOT NULL,
 CONSTRAINT [PK_ColorScheme] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[countries]    Script Date: 22.04.2019 21:41:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[countries](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_countries] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[directors]    Script Date: 22.04.2019 21:41:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[directors](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[first_name] [nvarchar](max) NOT NULL,
	[second_name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_directors] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[film_marks]    Script Date: 22.04.2019 21:41:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[film_marks](
	[idfilm] [int] NOT NULL,
	[iduser] [int] NOT NULL,
	[user_mark] [int] NULL,
 CONSTRAINT [PK_film_marks] PRIMARY KEY CLUSTERED 
(
	[idfilm] ASC,
	[iduser] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[films]    Script Date: 22.04.2019 21:41:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[films](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](max) NOT NULL,
	[year] [int] NOT NULL,
	[country] [int] NOT NULL,
	[director] [int] NOT NULL,
	[description] [nvarchar](max) NULL,
	[trailer] [nvarchar](max) NULL,
	[rating] [float] NULL,
 CONSTRAINT [PK_Film] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[genres]    Script Date: 22.04.2019 21:41:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[genres](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_genres] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[genres_in_films]    Script Date: 22.04.2019 21:41:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[genres_in_films](
	[film_id] [int] NOT NULL,
	[genre_id] [int] NOT NULL,
 CONSTRAINT [PK_genres_in_films] PRIMARY KEY CLUSTERED 
(
	[film_id] ASC,
	[genre_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[offers]    Script Date: 22.04.2019 21:41:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[offers](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[offer] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_offers] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Roles]    Script Date: 22.04.2019 21:41:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[id] [int] NOT NULL,
	[role] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[statuses]    Script Date: 22.04.2019 21:41:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[statuses](
	[id] [int] NOT NULL,
	[status] [nvarchar](50) NULL,
 CONSTRAINT [PK_statuses] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[users]    Script Date: 22.04.2019 21:41:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[login] [nvarchar](max) NOT NULL,
	[password] [nvarchar](max) NOT NULL,
	[salt] [nvarchar](max) NOT NULL,
	[idRole] [int] NULL,
	[idStatus] [int] NULL,
	[idColorScheme] [int] NULL,
 CONSTRAINT [PK_users] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[ColorScheme] ON 

INSERT [dbo].[ColorScheme] ([id], [background], [maincolor], [additional], [mapAdditional], [mapActive]) VALUES (1, N'#fce5bf', N'#8be5d3', N'#c7d3f9', N'#ff80ff', N'#ffff00   ')
INSERT [dbo].[ColorScheme] ([id], [background], [maincolor], [additional], [mapAdditional], [mapActive]) VALUES (2, N'#fce5bf', N'#8be5d3', N'#c7d3f9', N'#ff80ff', N'#ffff00   ')
INSERT [dbo].[ColorScheme] ([id], [background], [maincolor], [additional], [mapAdditional], [mapActive]) VALUES (3, N'#ffffff', N'#e8747d', N'#aadbff', N'#00ff00', N'#c0c0c0   ')
INSERT [dbo].[ColorScheme] ([id], [background], [maincolor], [additional], [mapAdditional], [mapActive]) VALUES (5, N'#000000', N'#8080ff', N'#008000', N'#804000', N'#000000   ')
INSERT [dbo].[ColorScheme] ([id], [background], [maincolor], [additional], [mapAdditional], [mapActive]) VALUES (6, N'#fce5bf', N'#8be5d3', N'#c7d3f9', N'#efffef', N'#ffff00   ')
SET IDENTITY_INSERT [dbo].[ColorScheme] OFF
SET IDENTITY_INSERT [dbo].[countries] ON 

INSERT [dbo].[countries] ([id], [name]) VALUES (1, N'США')
INSERT [dbo].[countries] ([id], [name]) VALUES (2, N'Россия')
INSERT [dbo].[countries] ([id], [name]) VALUES (3, N'Франция')
INSERT [dbo].[countries] ([id], [name]) VALUES (4, N'Германия')
INSERT [dbo].[countries] ([id], [name]) VALUES (5, N'Япония')
INSERT [dbo].[countries] ([id], [name]) VALUES (6, N'Великобритания')
SET IDENTITY_INSERT [dbo].[countries] OFF
SET IDENTITY_INSERT [dbo].[directors] ON 

INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (1, N'Джонатан', N'Демми')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (2, N'Фрэнк', N'Дарабонт')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (3, N'Питер', N'Джексон')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (4, N'Кристофер', N'Нолан')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (5, N'Рич', N'Мур')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (6, N'Джузеппе', N'Торнаторе')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (7, N'Люк', N'Бессон')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (8, N'Кристоф', N'Барратье')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (9, N'Рави', N'Чопра')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (10, N'Оливье', N'Накаш')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (11, N'Клод', N'Баррас')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (12, N'Николай', N'Досталь')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (13, N'Владимир', N'Макеранец')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (14, N'Сергей', N'Говорухин')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (15, N'Александр', N'Татарский')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (16, N'Сергей', N'Меринов')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (17, N'Елена', N'Погребижская')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (18, N'Николай', N'Лебедев')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (19, N'Роберт', N'Олтмен')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (20, N'Бела', N'Тарр')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (21, N'Дэвид', N'Финчер')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (22, N'Пол', N'МакГиган')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (23, N'Клинт', N'Иствуд')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (24, N'Фархан', N'Ахтар')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (25, N'Декстер', N'Флетчер')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (26, N'Акира', N'Куросава')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (27, N'Ёсифуми', N'Кондо')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (28, N'Масао', N'Курода')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (29, N'Хаяо', N'Миядзаки')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (30, N'Йодзиро', N'Такита')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (31, N'Ёити', N'Фудзита')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (32, N'Макото', N'Синкай')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (33, N'Кевин', N'Костнер')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (34, N'Дэнни', N'Бойл')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (35, N'Ридли', N'Скотт')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (36, N'Крис', N'Коламбус')
INSERT [dbo].[directors] ([id], [first_name], [second_name]) VALUES (37, N'Джон', N'Карни')
SET IDENTITY_INSERT [dbo].[directors] OFF
SET IDENTITY_INSERT [dbo].[films] ON 

INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (1, N'Молчание ягнят', 1990, 1, 1, N'Психопат похищает и убивает молодых женщин по всему Среднему Западу Америки. ФБР, уверенное в том, что все преступления совершены одним и тем же человеком, поручает агенту Клариссе Стерлинг встретиться с заключенным-маньяком, который мог бы объяснить следствию психологические мотивы серийного убийцы и тем самым вывести на его след.', N'iZvDUsUdS8I', 8.3)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (2, N'Побег из Шоушенка', 1994, 1, 2, N'Успешный банкир Энди Дюфрейн обвинен в убийстве собственной жены и ее любовника. Оказавшись в тюрьме под названием Шоушенк, он сталкивается с жестокостью и беззаконием, царящими по обе стороны решетки. Каждый, кто попадает в эти стены, становится их рабом до конца жизни. Но Энди, вооруженный живым умом и доброй душой, отказывается мириться с приговором судьбы и начинает разрабатывать невероятно дерзкий план своего освобождения.', N'87Pxs5MSr_c', 9.1)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (3, N'Зеленая миля
', 1999, 1, 2, N'Обвиненный в страшном преступлении, Джон Коффи оказывается в блоке смертников тюрьмы «Холодная гора». Вновь прибывший обладал поразительным ростом и был пугающе спокоен, что, впрочем, никак не влияло на отношение к нему начальника блока Пола Эджкомба, привыкшего исполнять приговор.
', N'c8G1144UMcM', 9)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (4, N'Властелин колец Возвращение Короля
', 2003, 1, 3, N'Последняя часть трилогии о Кольце Всевластия и о героях, взявших на себя бремя спасения Средиземья. Повелитель сил Тьмы Саурон направляет свои бесчисленные рати под стены Минас-Тирита, крепости Последней Надежды. Он предвкушает близкую победу, но именно это и мешает ему заметить две крохотные фигурки — хоббитов, приближающихся к Роковой Горе, где им предстоит уничтожить Кольцо Всевластия. Улыбнется ли им счастье?', N'QVIt5n94LL0', 8.6)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (5, N'Начало
', 2010, 1, 4, N'﻿Доминик Кобб – единственный, кто может не просто залезть в чужой сон и узнать все секреты, но также внушить человеку любую идею. Именно этого от специалиста по внедрению хочет японский бизнесмен Сайто, чтобы развалить империю конкурента, доставшуюся в наследство его сыну.Кобб набирает команду и устраивает многоуровневую игру в подсознании Роберта Фишера, причем главную опасность в реальности сновидений для Кобба представляет его покойная жена Мол, с которой он когда-то провел трюк по внедрению, теперь он не может полностью контролировать происходящее.', N'85Zz1CCXyDI', 8.6)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (6, N'Интерстеллар
', 2014, 1, 4, N'Когда засуха приводит человечество к продовольственному кризису, коллектив исследователей и учёных отправляется сквозь червоточину (которая предположительно соединяет области пространства-времени через большое расстояние) в путешествие, чтобы превзойти прежние ограничения для космических путешествий человека и переселить человечество на другую планету.', N'qcPfI0y7wRU', 8.5)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (7, N'Зверополис
', 2016, 1, 5, N'Добро пожаловать в Зверополис — современный город, населенный самыми разными животными, от огромных слонов до крошечных мышек. Зверополис разделен на районы, полностью повторяющие естественную среду обитания разных жителей — здесь есть и элитный район Площадь Сахары и неприветливый Тундратаун. В этом городе появляется новый офицер полиции, жизнерадостная зайчиха Джуди Хоппс, которая с первых дней работы понимает, как сложно быть маленькой и пушистой среди больших и сильных полицейских. Джуди хватается за первую же возможность проявить себя, несмотря на то, что ее партнером будет болтливый хитрый лис Ник Уайлд. Вдвоем им предстоит раскрыть сложное дело, от которого будет зависеть судьба всех обитателей Зверополиса.', N'oqFqf-1NcEc', 8.3)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (8, N'Облако-рай
', 1990, 2, 12, N'Скучная, тусклая, серая однообразность провинциального городка. Чтобы обратить на себя внимание, молодой парень Николай «ляпнул», что уезжает к другу на дальний восток. Что тут началось! Весь город принимает участие в сборах. Да с каким грандиозным размахом, словно едут всем городом.

И даже чистосердечное признание Николая, что все это — вранье, да и друга-то нет, не могут никого остановить. С помпой, в торжественной обстановке, Николая с музыкой выпроваживают из города…', N'Egs2EP9QQ9A', 8)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (9, N'Ты есть...
', 1993, 2, 13, N'Ситуация, в которой оказалась героиня фильма Анна, стара как мир. Единственный сын после смерти мужа стал смыслом ее существования. Но однажды он приводит в дом симпатичную девушку Иру, а утром сообщает матери, что они поженились. Анну переполняют ревность и страх потерять сына.

Натянутые отношения «свекровь — невестка» быстро перерастают в конфликт. Молодые уходят из дома. Анна мучительно переживает одиночество. А через пару недель ребята попадают в автомобильную аварию. Сын не пострадал, а его молодая жена стала калекой, прикованной к постели. Из больницы сын приносит жену домой, к матери. Забыв обиду, Анна соглашается помочь…', NULL, 7.9)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (10, N'Прокляты и забыты
', 1997, 2, 14, N'
Сегодняшние войны неправедны. На них воюют обреченные. Несколько тысяч обреченных в многомиллионной стране. Уцелевшим, вернувшимся с войны солдатам уже не вписаться в эту действительность. Жившим по законам совести, им не найти места на этой ярмарке тщеславия. И потому их война не окончена.

Для чего я говорю об этом? Что пытаюсь доказать? Мир давно сузился до размеров малогабаритного счастья и границы его непоколебимы. Мы были гражданами страны. Плохой, хорошей ли, но страны. Мы стали гражданами квартиры.', NULL, 8.5)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (11, N'Гора самоцветов
', 2005, 2, 15, N'Сказки народов России.', N'zlqKkO7c2ck', 8.1)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (12, N'Гора самоцветов 3
', 2008, 2, 16, N'Сказка — первый и самый древний путь познания мира. «Гора самоцветов» — это пять анимационных фильмов сделанных в разнообразных жанрах и посвященных одному из многочисленных народов России. Каждую серию из цикла представляют одни и те же слова — «Мы живем в России». Всего несколько добрых слов о каждом народе, а следом — его мудрая и самобытная сказка.', N'Q3oM3kvfs1I', 8)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (13, N'Мама, я убью тебя
', 2013, 2, 17, N'Сашка, Настя и Леха проведут в интернате все свое детство. Интернат больше всего похож на детскую тюрьму, хотя взрослые, которые там работают, убеждены, что делают детей счастливыми.', N'L0DWLSZ9jDU', 8.1)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (14, N'Экипаж
', 2016, 2, 18, N'История талантливого молодого летчика Алексея Гущина. Он не признает авторитетов, предпочитая поступать в соответствии с личным кодексом чести. За невыполнение абсурдного приказа его выгоняют из военной авиации, и только чудом он получает шанс летать на гражданских самолетах.

Гущин начинает свою летную жизнь сначала. Его наставник — командир воздушного судна — суровый и принципиальный Леонид Зинченко. Его коллега — второй пилот, неприступная красавица Александра. Отношения складываются непросто. Но на грани жизни и смерти, когда земля уходит из-под ног, вокруг — огонь и пепел и только в небе есть спасение, Гущин показывает все, на что он способен. Только вместе экипаж сможет совершить подвиг и спасти сотни жизней.', N'5nlaow5dmk0', 7.6)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (15, N'У них все хорошо
', 1990, 3, 6, N'70-летний отец отправляется навестить своих пятерых детей, чтобы убедиться, что у них все хорошо. Они не хотят его разочаровывать…', NULL, 7.7)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (16, N'Леон
', 1994, 3, 7, N'Профессиональный убийца Леон, не знающий пощады и жалости, знакомится со своей очаровательной соседкой Матильдой, семью которой расстреливают полицейские, замешанные в торговле наркотиками. Благодаря этому знакомству он впервые испытывает чувство любви, но…', N'9XZ1kCx3O3I4', 8.6)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (17, N'Пятый элемент
', 1997, 3, 7, N'Каждые пять тысяч лет открываются двери между измерениями и темные силы стремятся нарушить существующую гармонию. Каждые пять тысяч лет Вселенной нужен герой, способный противостоять этому злу. XXIII век. Нью-йоркский таксист Корбен Даллас должен решить глобальную задачу — спасение всего рода человеческого.

Зло в виде раскаленной массы, наделенной интеллектом, надвигается на Землю. Победить его можно, только лишь собрав воедино четыре элемента (они же стихии — земля, вода, воздух и огонь) и добавив к ним загадочный пятый элемент.', N'mjEfnCQnmR4', 8)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (18, N'Хористы
', 2004, 3, 8, N'Франция, 1949 год. Отчаявшись найти работу, учитель музыки Клемент Матье попадает в интернат для трудных подростков. Там он видит, к каким жестоким «воспитательным мерам» прибегает ректор этого заведения Рашан. Чем больше издевается Рашан над мальчиками, тем агрессивнее они становятся.

Добродушного по натуре Матье возмущают эти методы, но он не в состоянии открыто протестовать. Но однажды ему, автору многочисленных музыкальных произведений, которые он скромно прячет в своей комнате, приходит в голову замечательная идея: организовать школьный хор.', N'ADnRnZjdS28', 8.2)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (20, N'Папа
', 2006, 3, 9, N'Балрай Капур — богатый бизнесмен с прогрессивными взглядами на жизнь. Для него семейное благополучие — главная ценность. Шобхна — намного больше, чем просто лучшая половина Бапрая. Вместе они всеми силами опекают и лелеют своего единственного ребенка, Авинаша, который Балраю скорее друг, чем просто сын.

Авинаш много лет провел за рубежом, получая образование. И вот, долгожданный сын возвращается домой на радость родителям. Вскоре он влюбляется в прелестную художницу Милли, их счастливый брак вознаграждается милым ребенком.

У Милли есть друг, музыкант Раджат, он давно любит ее и скрывает это. Видя счастливую семью Авинаша и Милли, он решает перебраться в Европу, чтобы там строить свою музыкальную карьеру.

Однако судьба готовит крутой поворот — Авинаш трагически погибает. Жизнь Милли становится невыносимой без мужа. Балрай из любви к внуку и невестке, вопреки устоявшимся традициям, вопреки непониманию со стороны брата Балвантэ и собственной жены Шобхны, пытается устроить жизнь Миппи, вернуть ей счастье и радость…', N'3d-d1REq1EU', 8.1)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (21, N'Intouchables', 2011, 3, 10, N'Пострадав в результате несчастного случая, богатый аристократ Филипп нанимает в помощники человека, который менее всего подходит для этой работы, — молодого жителя предместья Дрисса, только что освободившегося из тюрьмы. Несмотря на то, что Филипп прикован к инвалидному креслу, Дриссу удается привнести в размеренную жизнь аристократа дух приключений.', N'aMEYTIt6xzU', 8.8)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (23, N'Жизнь Кабачка
', 2016, 3, 11, N'Кабачок — вовсе не овощ, так зовут маленького мальчика. Его мама погибла, и он остался во всем мире совсем один и совсем не знает, как и с кем ему жить дальше. Симон, Ахмед, Жужубе, Алиса и Беатрис — новые друзья Кабачка, которых он встречает в приюте для детей. Там же он впервые влюбится, в особенную, очаровательную Камиллу.

Это история о детях, жизнь которых полна больших и маленьких потрясений, но они находят в себе силы снова доверять людям, продолжают любить мир, радуются всему новому.', N'-VEQ92UQ_FI', 7.3)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (24, N'Винсент и Тео
', 1990, 4, 19, N'На занятия живописью Ван Гогу было отмерено всего лишь одно десятилетие, однако за это время он написал около 800 картин и прошел путь от неуверенного новичка до зрелого мастера, одержимого собственным искусством и сжигающего ради него все мосты.', N'IiSeRzUhmhE', 7.4)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (25, N'Сатанинское танго
', 1994, 4, 20, N'Действие фильма разворачивается на территории фермы, доживающей свои последние дни. Несколько ее жителей решают уйти, похитив деньги, вырученные всеми участниками коммуны перед ее закрытием. Однако их планы нарушают слухи о появлении красноречивого и харизматичного Иримиаша, пропавшего полтора года назад и считавшегося погибшим.', N'eQ66STMnBt0', 7.9)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (26, N'Бойцовский клуб
', 1999, 4, 21, N'Терзаемый хронической бессонницей и отчаянно пытающийся вырваться из мучительно скучной жизни, клерк встречает некоего Тайлера Дардена, харизматического торговца мылом с извращенной философией. Тайлер уверен, что самосовершенствование — удел слабых, а саморазрушение — единственное, ради чего стоит жить.

Пройдет немного времени, и вот уже главные герои лупят друг друга почем зря на стоянке перед баром, и очищающий мордобой доставляет им высшее блаженство. Приобщая других мужчин к простым радостям физической жестокости, они основывают тайный Бойцовский Клуб, который имеет огромный успех. Но в концовке фильма всех ждет шокирующее открытие, которое может привести к непредсказуемым событиям…', N'oqHJp_ZZdU4', 8.6)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (27, N'Счастливое число Слевина
', 2005, 4, 22, N'Слевину не везет. Дом опечатан, девушка ушла к другому… Его друг Ник уезжает из Нью-Йорка и предлагает Слевину пожить в пустой квартире. В это время крупный криминальный авторитет по прозвищу Босс хочет рассчитаться со своим бывшим партнером за убийство сына и в отместку «заказать» его наследника.', N'sfCw4lS9EkA', 8.1)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (28, N'Гран Торино
', 2008, 4, 23, N'Вышедший на пенсию автомеханик Уолт Ковальски проводит дни, починяя что-то по дому, попивая пиво и раз в месяц заходя к парикмахеру. И хотя последним желанием его недавно почившей жены было совершение им исповеди, Уолту — ожесточившемуся ветерану Корейской войны, всегда держащему свою винтовку наготове, — признаваться в общем-то не в чем. Да и нет того, кому он доверял бы в той полной мере, в какой доверяет своей собаке Дейзи.

Все те люди, коих он некогда называл своими соседями, либо переехали, либо ушли в мир иной, а на смену им пришли иммигранты, которых он откровенно презирает. Противясь буквально всему, во что упирается его взгляд, — покосившиеся карнизы, заросшие лужайки домов и лица иностранцев повсюду; бесцельно шатающиеся шайки подростков, состоящие из хмонгов, латиносов и афро-американцев, полагающих, что окрестный мир целиком принадлежит им одним; неоперившиеся чужаки, в которых оказалось превратились собственные дети и внуки, — Уолт просто неспешно проживает остаток своей жизни.

До той самой ночи, когда кто-то попытается украсть его «Гран Торино»…', N'j-1JVkvwV18', 8.1)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (29, N'Дон. Главарь мафии 2
', 2011, 4, 24, N'Дону уже принадлежит вся Азия. Однако ему этого мало: он задумал покорить Европу и решил начать с Берлина. Как обычно на его пути стоит спецотряд полиции и многочисленные конкуренты.. ……', N'LAFIPSP4v4s', 8.1)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (30, N'Эдди «Орел»
', 2016, 4, 25, N'Фильм повествует о трамплинисте Эдди, по прозвищу «Орел». Он прославился благодаря своим неудачам, ибо на всех соревнованиях он занимал последние места.', N'QxWuxLaaw7Y', 7.5)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (31, N'Сны Акиры Куросавы
', 1990, 5, 26, N'Восемь новелл, связанных лишь тем, что они являются «снами» режиссёра. Среди них рассказ о том, как молодой Куросава попадает в картину Винсента Ван Гога и встречается там с самим автором. Также рассказы о жертвах Второй мировой и об апокалиптических картинах ядерной войны.

В одной из новелл, повествующей о лисьей свадьбе, домик, на фоне которого стоит герой — маленький мальчик, является воспоминанием Куросавы о доме, в котором он жил в детстве. Весь фильм во многом автобиографичен.', N'fq9p7EgSQjY', 7.7)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (32, N'Шёпот сердца
', 1995, 5, 27, N'Сидзуку, взяв очередную книгу в библиотеке, случайно заметила, что до нее эту книгу взял некий Сейдзи. И это произошло не впервые, этот Сейдзи читал и другие книги, которые понравились ей. Заинтересовавшись, Сидзуку решила узнать — кто он, этот читающий принц?', N'u7oAjgeCT98', 8.3)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (33, N'Фландрийский пёс', 1997, 5, 28, N'У мальчика Нелло, наделенного даром рисования, живет пес Патраш, добрый и симпатичный. С мальчиком дружит Алуа, дочь богатого человека, но папа не разрешает ей с ним дружить, считая, что бедный мальчик для нее не подходящая компания. А Нелло так хорошо нарисовал ее портрет. Однажды он, его дедушка и Алуа пошли в Антверпен, там Патраша узнал его бывший злой хозяин, захотевший взять пса обратно. Но Нелло и его друзья помешали этому. Мальчик принял участие в конкурсе юных художников — победитель мог бесплатно учиться в школе искусств. О жизни Нелло, его пса Патраша, Алуа и рассказывает этот фильм.', NULL, 8.6)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (34, N'Унесённые призраками
', 2001, 5, 29, N'Маленькая Тихиро вместе с мамой и папой переезжают в новый дом. Заблудившись по дороге, они оказываются в странном пустынном городе, где их ждет великолепный пир. Родители с жадностью набрасываются на еду и к ужасу девочки превращаются в свиней, став пленниками злой колдуньи Юбабы, властительницы таинственного мира древних богов и могущественных духов. 

Теперь, оказавшись одна среди магических существ и загадочных видений, отважная Тихиро должна придумать, как избавить своих родителей от чар коварной старухи и спастись из пугающего царства призраков.', N'BZWp-RcqIM4', 8.4)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (35, N'Ушедшие
', 2008, 5, 30, N'Главный герой картины — молодой виолончелист Дайго, оставшийся без работы. Вместе с женой он перебирается из мегаполиса в родной городок в провинции, где ему предложили работу в некоем агентстве, как будто туристическом. Однако, оказывается, это — похоронное бюро, и Дайго предстоит обмывать тела покойников, наряжать их и готовить к переходу в лучший мир. С большим удивлением герой понимает, что справляется со своей новой работой очень хорошо.', N'4FXv67XlGjg', 8)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (36, N'Гинтама 2
', 2013, 5, 31, N'Гинтоки применяет таинственную силу и попадает в будущее, где его не будет. Этот будущий мир очень странный и хаотичный. Гинтоки в этом хаосе встречает разных существ, а также Шинпачи и Кагуру. Гинтоки попадает в непредвиденную цепочку событий, где ему открывается ошеломляющая правда.', N'HNAQhcMFiIw', 8.5)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (37, N'Твоё имя
', 2016, 5, 32, N'История о парне из Токио и девушке из провинции, которые обнаруживают, что между ними существует странная и необъяснимая связь. Во сне они меняются телами и проживают жизни друг друга. Но однажды эта способность исчезает так же внезапно, как появилась. Таки решает во что бы то ни стало отыскать Мицуху, но способны ли они узнать друг друга в реальной жизни?', N'tT7b5wR0IOM', 8.3)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (38, N'Танцующий с волками
', 1990, 6, 33, N'Действие фильма происходит в США во времена Гражданской войны. Лейтенант американской армии Джон Данбар после ранения в бою просит перевести его на новое место службы ближе к западной границе США. Место его службы отдалённый маленький форт. Непосредственный его командир покончил жизнь самоубийством, а попутчик Данбара погиб в стычке с индейцами после того, как довез героя до места назначения. Людей, знающих, что Данбар остался один в форте и должен выжить в условиях суровой природы, и в соседстве с кажущимися негостеприимными коренными обитателями Северной Америки, просто не осталось. Казалось, он покинут всеми. Постепенно лейтенант осваивается, он ведет записи в дневнике…', N'DUHevygtIcc', 8.1)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (39, N'На игле
', 1995, 6, 34, N'Выбирай жизнь. Выбирай работу. Выбирай карьеру. Выбирай семью. Выбирай большие телевизоры, стиральные машины, автомобили, компакт-диск плееры, электрические консервные ножи. Выбирай хорошее здоровье, низкий уровень холестерина и стоматологическую страховку.

Выбирай недвижимость и аккуратно выплачивай взносы. Выбери свой первый дом. Выбирай своих друзей. Выбери себе курорт и шикарные чемоданы. Выбери костюм-тройку лучшей фирмы из самого дорогого материала. Выбери набор «Сделай сам», чтобы было чем заняться воскресным утром.

Выбери удобный диван, чтобы развалиться на нем и смотреть отупляющее шоу. Набивай свое брюхо всякой всячиной. Выбери загнивание в конце всего и вспомни со стыдом напоследок своих дружков-подонков, которых ты заложил чтобы выкарабкаться. Выбирай будущее. Выбирай жизнь.', N'RLJWg54eLoI', 7.9)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (40, N'Гладиатор
', 2000, 6, 35, N'В великой Римской империи не было военачальника, равного генералу Максимусу. Непобедимые легионы, которыми командовал этот благородный воин, боготворили его и могли последовать за ним даже в ад.

Но случилось так, что отважный Максимус, готовый сразиться с любым противником в честном бою, оказался бессилен против вероломных придворных интриг. Генерала предали и приговорили к смерти. Чудом избежав гибели, Максимус становится гладиатором.

Быстро снискав себе славу в кровавых поединках, он оказывается в знаменитом римском Колизее, на арене которого он встретится в смертельной схватке со своим заклятым врагом…', N'UIenlyv_MeE', 8.5)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (41, N'Гарри Поттер и философский камень
', 2001, 6, 36, N'Жизнь десятилетнего Гарри Поттера нельзя назвать сладкой: его родители умерли, едва ему исполнился год, а от дяди и тётки, взявших сироту на воспитание, достаются лишь тычки да подзатыльники. Но в одиннадцатый день рождения Гарри всё меняется. Странный гость, неожиданно появившийся на пороге, приносит письмо, из которого мальчик узнаёт, что на самом деле он волшебник и принят в Хогвартс — школу магии. А уже через пару недель Гарри будет мчаться в поезде Хогвартс-экспресс навстречу новой жизни, где его ждут невероятные приключения, верные друзья и самое главное — ключ к разгадке тайны смерти его родителей.', N'5sL_0KvRG40', 8.1)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (43, N'Престиж
', 2006, 6, 4, N'Роберт и Альфред — фокусники-иллюзионисты, которые на рубеже XIX и XX веков соперничали друг с другом в Лондоне. С годами их дружеская конкуренция на профессиональной почве перерастает в настоящую войну.

Они готовы на все, чтобы выведать друг у друга секреты фантастических трюков и сорвать их исполнение. Непримиримая вражда, вспыхнувшая между ними, начинает угрожать жизни окружающих их людей…', N'0B6O17m9sWE', 8.5)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (44, N'Темный рыцарь Возрождение легенды
', 2012, 6, 4, N'Восемь лет назад Бэтмен растворился в ночи, превратившись из героя в беглеца. Приняв на себя вину за смерть прокурора Харви Дента, он пожертвовал всем. Вместе с комиссаром Гордоном они решили, что так будет лучше для всех. Пока преступность была раздавлена антикриминальным актом Дента, ложь действовала.

Тем не менее, еще опаснее становится появление нового врага Бэйна, чье лицо закрыто маской. Он разворачивает в Готэме чудовищную деятельность, и это вынуждает Брюса Уэйна выйти из импровизированного изгнания. Однако даже надев свой костюм, Бэтмен может проиграть Бэйну. Но с появлением хитрой воровки по прозвищу Женщина — Кошка с загадочным прошлым всё меняется.', N'qXaP7e7ByUg', 8.1)
INSERT [dbo].[films] ([id], [name], [year], [country], [director], [description], [trailer], [rating]) VALUES (45, N'Синг Стрит
', 2016, 6, 37, N'Ностальгическая музыкальная драма о взрослении. Когда экономический кризис 1980-х ударяет по семье Конора, его переводят из престижной частной школы в государственную. Подросток сталкивается с грубостью и невежеством новой среды и, кроме того, влюбляется в недосягаемую красавицу. Желая впечатлить девушку, Конор собирает рок-группу.', N'DVju1Y0Pe8w', 7.6)
SET IDENTITY_INSERT [dbo].[films] OFF
SET IDENTITY_INSERT [dbo].[genres] ON 

INSERT [dbo].[genres] ([id], [name]) VALUES (1, N'Драма')
INSERT [dbo].[genres] ([id], [name]) VALUES (2, N'Криминал')
INSERT [dbo].[genres] ([id], [name]) VALUES (3, N'Боевик')
INSERT [dbo].[genres] ([id], [name]) VALUES (4, N'Аниме')
INSERT [dbo].[genres] ([id], [name]) VALUES (5, N'Фантастика')
INSERT [dbo].[genres] ([id], [name]) VALUES (6, N'Фэнтези')
INSERT [dbo].[genres] ([id], [name]) VALUES (7, N'Детектив')
INSERT [dbo].[genres] ([id], [name]) VALUES (8, N'Триллер')
INSERT [dbo].[genres] ([id], [name]) VALUES (9, N'Комедия')
INSERT [dbo].[genres] ([id], [name]) VALUES (10, N'Приключения')
INSERT [dbo].[genres] ([id], [name]) VALUES (11, N'Мультфильм')
INSERT [dbo].[genres] ([id], [name]) VALUES (12, N'Документальный')
INSERT [dbo].[genres] ([id], [name]) VALUES (13, N'Семейный')
INSERT [dbo].[genres] ([id], [name]) VALUES (14, N'Музыка')
INSERT [dbo].[genres] ([id], [name]) VALUES (15, N'Биография')
INSERT [dbo].[genres] ([id], [name]) VALUES (17, N'Вестерн')
SET IDENTITY_INSERT [dbo].[genres] OFF
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (1, 2)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (1, 8)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (2, 1)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (2, 2)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (3, 1)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (3, 2)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (4, 5)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (4, 10)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (5, 1)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (5, 3)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (6, 5)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (6, 10)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (7, 10)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (7, 11)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (8, 1)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (8, 9)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (9, 1)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (10, 12)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (11, 11)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (12, 11)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (13, 12)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (13, 13)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (14, 8)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (14, 10)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (15, 1)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (16, 2)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (16, 8)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (17, 5)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (17, 9)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (18, 1)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (18, 14)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (20, 1)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (20, 13)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (21, 1)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (21, 9)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (23, 11)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (23, 13)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (24, 1)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (24, 15)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (25, 1)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (26, 2)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (26, 8)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (27, 7)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (27, 8)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (28, 1)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (29, 2)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (29, 3)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (30, 9)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (30, 15)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (31, 1)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (31, 6)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (32, 1)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (32, 4)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (33, 1)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (33, 4)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (34, 4)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (34, 6)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (35, 1)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (35, 14)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (36, 4)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (36, 9)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (37, 4)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (37, 6)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (38, 10)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (38, 17)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (39, 1)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (40, 3)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (40, 10)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (41, 6)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (41, 13)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (43, 5)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (43, 7)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (44, 3)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (44, 8)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (45, 9)
INSERT [dbo].[genres_in_films] ([film_id], [genre_id]) VALUES (45, 14)
SET IDENTITY_INSERT [dbo].[offers] ON 

INSERT [dbo].[offers] ([id], [offer]) VALUES (1, N'Не воспроизводит трейлер к фильму "Бумер"')
SET IDENTITY_INSERT [dbo].[offers] OFF
INSERT [dbo].[Roles] ([id], [role]) VALUES (1, N'Юзер')
INSERT [dbo].[Roles] ([id], [role]) VALUES (2, N'Админ')
INSERT [dbo].[statuses] ([id], [status]) VALUES (1, N'Забанен')
SET IDENTITY_INSERT [dbo].[users] ON 

INSERT [dbo].[users] ([id], [login], [password], [salt], [idRole], [idStatus], [idColorScheme]) VALUES (16, N'1', N'xmNE2tNPMwXmJRxaqHxSJw==', N'5+qsO9BtmqnrwcTzQOF6txvojhq1', 1, NULL, 3)
INSERT [dbo].[users] ([id], [login], [password], [salt], [idRole], [idStatus], [idColorScheme]) VALUES (22, N'mark', N'Of5Je46obVOXAr9JkKGDcQ==', N'jksFAAz8SRKT+IMTvNo9rFPDBGbRr69WbCPrH+8=', 2, NULL, 6)
INSERT [dbo].[users] ([id], [login], [password], [salt], [idRole], [idStatus], [idColorScheme]) VALUES (23, N'" insert into users values (''1'', ''1'', ''1'')', N'9kpXRoN7X62YD1ihJfIR9A==', N'rLPLGJhMLB1R7rJWN0yRi4MFBI6E5Ct8', 1, 1, NULL)
INSERT [dbo].[users] ([id], [login], [password], [salt], [idRole], [idStatus], [idColorScheme]) VALUES (24, N'" insert into users values (''1'', ''1'', ''1'')"', N'vv5hyW7nxUAldzze+hWA4A==', N'oIw3DB5N52MbIxvVm24DBGIL4DaNYQ==', 1, 1, NULL)
INSERT [dbo].[users] ([id], [login], [password], [salt], [idRole], [idStatus], [idColorScheme]) VALUES (25, N'dead1', N'ejtUnLbwRdTngHqMKS2haw==', N'2a5GXuw8KSIxFQYycDfRg0e6yI98LbE=', 1, 1, NULL)
INSERT [dbo].[users] ([id], [login], [password], [salt], [idRole], [idStatus], [idColorScheme]) VALUES (27, N'dead3', N'bYssRI9kxzbBD7hn+h0VYg==', N'n+BX+IXuOOz/gExKHnrLpOYdLc5XMw==', 1, NULL, NULL)
INSERT [dbo].[users] ([id], [login], [password], [salt], [idRole], [idStatus], [idColorScheme]) VALUES (28, N'dead4', N'HM52mLhKxoCEVLj2qILMAQ==', N'WOslQb5PvRlcRDBAAnp6xQHYGRJYfCtnn6Y2', 1, NULL, NULL)
INSERT [dbo].[users] ([id], [login], [password], [salt], [idRole], [idStatus], [idColorScheme]) VALUES (31, N'test1', N'mWhpNipLypEHMJho250g7A==', N'I5OoYkudndE48bMLDFRSJ0zbOJYaaLo=', 1, NULL, 5)
INSERT [dbo].[users] ([id], [login], [password], [salt], [idRole], [idStatus], [idColorScheme]) VALUES (32, N'bubu', N'MUvyHWgu90CurGk2nWcTmQ==', N'Vn30Ik33/tp8F3Qop2srosBN9uwzt5uspgVbnpA=', 1, NULL, NULL)
INSERT [dbo].[users] ([id], [login], [password], [salt], [idRole], [idStatus], [idColorScheme]) VALUES (33, N'wofwof', N'nqsSU5yVEBa360EuQl9eJg==', N'UWfQWkjE+UM1BhkK/r+pyafTbiF9zsM4', 1, NULL, NULL)
INSERT [dbo].[users] ([id], [login], [password], [salt], [idRole], [idStatus], [idColorScheme]) VALUES (34, N'wofwof2', N'wBk08Tviz/T0sYx/pqBojw==', N'MgEdgmOvVM4dGSX9lyh50+fjDDuYGTw7xw==', 1, NULL, NULL)
SET IDENTITY_INSERT [dbo].[users] OFF
ALTER TABLE [dbo].[film_marks]  WITH CHECK ADD  CONSTRAINT [FK_film_marks_films] FOREIGN KEY([idfilm])
REFERENCES [dbo].[films] ([id])
GO
ALTER TABLE [dbo].[film_marks] CHECK CONSTRAINT [FK_film_marks_films]
GO
ALTER TABLE [dbo].[film_marks]  WITH CHECK ADD  CONSTRAINT [FK_film_marks_users] FOREIGN KEY([iduser])
REFERENCES [dbo].[users] ([id])
GO
ALTER TABLE [dbo].[film_marks] CHECK CONSTRAINT [FK_film_marks_users]
GO
ALTER TABLE [dbo].[films]  WITH CHECK ADD  CONSTRAINT [FK_Film_countries] FOREIGN KEY([country])
REFERENCES [dbo].[countries] ([id])
GO
ALTER TABLE [dbo].[films] CHECK CONSTRAINT [FK_Film_countries]
GO
ALTER TABLE [dbo].[films]  WITH CHECK ADD  CONSTRAINT [FK_Film_directors] FOREIGN KEY([director])
REFERENCES [dbo].[directors] ([id])
GO
ALTER TABLE [dbo].[films] CHECK CONSTRAINT [FK_Film_directors]
GO
ALTER TABLE [dbo].[genres_in_films]  WITH CHECK ADD  CONSTRAINT [FK_genres_in_films_Film] FOREIGN KEY([film_id])
REFERENCES [dbo].[films] ([id])
GO
ALTER TABLE [dbo].[genres_in_films] CHECK CONSTRAINT [FK_genres_in_films_Film]
GO
ALTER TABLE [dbo].[genres_in_films]  WITH CHECK ADD  CONSTRAINT [FK_genres_in_films_genres] FOREIGN KEY([genre_id])
REFERENCES [dbo].[genres] ([id])
GO
ALTER TABLE [dbo].[genres_in_films] CHECK CONSTRAINT [FK_genres_in_films_genres]
GO
ALTER TABLE [dbo].[users]  WITH CHECK ADD  CONSTRAINT [FK_users_ColorScheme] FOREIGN KEY([idColorScheme])
REFERENCES [dbo].[ColorScheme] ([id])
GO
ALTER TABLE [dbo].[users] CHECK CONSTRAINT [FK_users_ColorScheme]
GO
ALTER TABLE [dbo].[users]  WITH CHECK ADD  CONSTRAINT [FK_users_Roles] FOREIGN KEY([idRole])
REFERENCES [dbo].[Roles] ([id])
GO
ALTER TABLE [dbo].[users] CHECK CONSTRAINT [FK_users_Roles]
GO
ALTER TABLE [dbo].[users]  WITH CHECK ADD  CONSTRAINT [FK_users_statuses] FOREIGN KEY([idStatus])
REFERENCES [dbo].[statuses] ([id])
GO
ALTER TABLE [dbo].[users] CHECK CONSTRAINT [FK_users_statuses]
GO
USE [master]
GO
ALTER DATABASE [dbFilmMap] SET  READ_WRITE 
GO
