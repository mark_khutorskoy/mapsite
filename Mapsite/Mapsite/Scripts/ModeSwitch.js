﻿var player;

// Инициализация
//Создание YT. плеера
jQuery('document').ready(function () {
    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";

    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    window.onYouTubeIframeAPIReady = function () {
        player = new YT.Player('trailer', {
            videoId: 'Div0iP65aZo',
            height: '195',
            width: '346',
        });
    }
    //Изначально трейлер скрыт
    jQuery('#trailer').hide()

});

//Свитчбокс

//скрыли трейлер 
//остановили трейлер
//показали постер
//включили музыку
jQuery('document').ready(function () {
    jQuery('#1').on('click', function () {

        //var myVid = document.getElementById("trailer");
        //myVid.pause();
        player.pauseVideo();

        jQuery('#trailer').hide();

        jQuery('#fig').show();

        var myAud = document.getElementById("music");
        myAud.play();
        

    });
});

//скрыли постер - запустили видео
//остановили музыку
//открыли видео
//запустили видео
jQuery('document').ready(function () {
    jQuery('#2').on('click', function () {
        jQuery('#fig').hide()

        var myAud = document.getElementById("music");
        myAud.pause();

        jQuery('#trailer').show();

        //if (player.videoId != null) {
            player.playVideo();
        //}
        //else alert("Трейлер недоступен");

    });
});

//ЗВУК
jQuery('document').ready(function () {
    jQuery('#soundbtn').on('click', function () {
        var myAud = document.getElementById("music");
        if (!jQuery("#soundbtn").prop("checked")) {
            myAud.volume = 0.0;
        }
        else {
            myAud.volume = 1.0;
        }
    });
});

//Изначально музыка ВЫКЛ
jQuery('document').ready(function () {
    
        var myAud = document.getElementById("music");
        myAud.volume = 0.0;
      
});

//Изначально logout скрыт
jQuery('document').ready(function () {
    jQuery('#btnLogout').hide();
});

//Изначально Юзер Контрол скрыт
jQuery('document').ready(function () {
    jQuery('#btnUsersControl').hide();
});

//Изначально смена цветов скрыта
jQuery('document').ready(function () {
    jQuery('#ChangeColors').hide();
});

//Изначально поиск скрыт
jQuery('document').ready(function () {
    jQuery('#SearchPage').hide();
});

//вызов подгруза видосика
function setvideo(id) {
    player.cueVideoById(id);
    //player.pauseVideo();
}