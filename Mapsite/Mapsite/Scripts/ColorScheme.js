﻿function SaveColors()
{
    myHub.server.saveColor($('#colorBackground').val(), $('#color1').val(), $('#color2').val(), $('#colorMap2').val(), $('#colorMapActive').val(), $('#lblUser').text());
}

function LoadColorInForm(background, color1, color2, colorMap2, colorMapActive)
{
    LoadColor(background, color1, color2, colorMap2, colorMapActive);
    $('body').css('background-color', background);
    $('.color-inside').css('background-color', color1);
    $('.top-menu button.active').css('background', color1);
    $('.color-inside-2').css('background-color', color2);
    $('button').css('background', color2);
    $('.top-menu button.active').css('background',color1); //костыль

    //здесь недоступных цвет
    var svgobject = document.getElementById('map');
    if ('contentDocument' in svgobject)
        var svgdom = svgobject.contentDocument;
    $(svgdom.getElementsByTagName("polygon")).attr('fill', color2);
    $(svgdom.getElementsByTagName("polyline")).attr('fill', color2);

    var style = '.area {fill: ' + colorMap2 + '}';
    var str = $('.area', svgdom).css('fill');
    var st = $('svg > style', svgdom).text().replace('.area {fill: ' + RGBtoHEX(str) + '}', "");
    $('svg > style', svgdom).text(st);
    $('svg > style', svgdom).prepend(style);

    var style = '.active {fill: ' + colorMapActive + '}';
    var str = $('.active', svgdom).css('fill');
    if (str != undefined) var st = $('svg > style', svgdom).text().replace('.active {fill: ' + RGBtoHEX(str) + '}', "");
    else var st = $('svg > style', svgdom).text().replace('.active {fill: #ffff00}', "");

    $('svg > style', svgdom).text(st);
    $('svg > style', svgdom).append(style);
}