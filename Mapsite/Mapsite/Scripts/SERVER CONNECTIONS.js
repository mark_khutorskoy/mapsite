﻿var myHub;
var searchHub;
function Initialize() {
    myHub = $.connection.myHub;
    myHub.client.alertFuncCl = function (mes) { alert(mes) };
    myHub.client.CloseReg = CloseReg;
    myHub.client.LoginChange = LoginChange;
    myHub.client.ErrorLoginLog = ErrorLoginLog;
    myHub.client.ErrorPasswordLog = ErrorPasswordLog;
    myHub.client.NotErrorLoginLog = NotErrorLoginLog;
    myHub.client.NotErrorPasswordLog = NotErrorPasswordLog;
    myHub.client.ErrorLoginReg = ErrorLoginReg;
    myHub.client.ErrorPasswordReg = ErrorPasswordReg;
    myHub.client.NotErrorLoginReg = NotErrorLoginReg;
    myHub.client.NotErrorPasswordReg = NotErrorPasswordReg;
    myHub.client.content = content;
    myHub.client.usersHTML = usersHTML;
    myHub.client.fillColors = LoadColorInForm;

    myHub.client.addCountry = addCountry;
    myHub.client.addGenre = addGenre;
    myHub.client.addFilmToPage = addFilmToPage;
    myHub.client.addRecommendations = addRecommendations;


    $.connection.hub.start().done(hubStarted);
    
}

function hubStarted() {
    $('#btnSignUp').click(Reg);
    $('#btnLogin').click(Log);
    $('#btnLogout').click(Logout);
    $('#tbLogin').change(CheckLogin);
    $('#tbPassword').change(CheckPassword);
    $('#tbRepeatPassword').change(CheckPassword);
    $('#btnUsersControl').click(getusers);
    $('#ban').click(banUsers);
    $('#Unban').click(UnbanUsers);
    $('#deleteUsers').click(deleteUsers);
    $('#saveScheme').click(SaveColors);
    myHub.server.Session();

    $('.radiobtns').change(getcontent);
    var svgobject = document.getElementById('map');
    if ('contentDocument' in svgobject) var svgdom = svgobject.contentDocument;
    $(svgdom.getElementsByClassName("area")).click(getcontent);

    $('document').ready(InitParams);
    $('#button-search').click(Search);


}