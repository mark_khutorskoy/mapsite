//var searchHub;

//function InitializeSearchParameters() {
//    searchHub = $.connection.searchHub;
//    searchHub.client.addCountry = addCountry;
//    searchHub.client.addGenre = addGenre;
//    $.connection.hub.start().done(searchHubStarted);
//}

//function hubParamsStarted() {
//    $('document').ready(InitParams);
//}

function InitParams() {
    //заполнение select'ов с годами
    for (var i = 1990; i < 2017; i += 5) {
        $('#selectYears1').append(new Option(i, i));
        $('#selectYears2').append(new Option(i, i));
        if (i == 2015) i -= 4;
    }
    $("#selectYears2 :last").attr("selected", "selected");

    //заполнение select'ов с рейтингом
    for (var i = 0; i < 11; i++) {
        $('#selectRating1').append(new Option(i, i));
        $('#selectRating2').append(new Option(i, i));
    }
    $("#selectRating2 :last").attr("selected", "selected");

    //изменение левой границы годов
    $('#selectYears1').on('change',
        function() {
            //запоминание правой границы
            var right = $('#selectYears2').val();

            $('#selectYears2').empty();
            for (var i = +$('#selectYears1 :selected').val(); i < 2017; i += 5) {
                $('#selectYears2').append(new Option(i, i));
                if (i == 2015) i -= 4;
            }

            $("#selectYears2 :contains(" + right + ")").attr("selected", "selected");
        });

    //изменение левой границы рейтинга
    $('#selectRating1').on('change',
        function() {
            //запоминание правой границы
            var right = $('#selectRating2').val();

            $('#selectRating2').empty();
            for (var i = +$('#selectRating1 :selected').val(); i < 11; i++)
                $('#selectRating2').append(new Option(i, i));

            $("#selectRating2 :contains(" + right + ")").attr("selected", "selected");
        });

    //загрузить чекбоксы со странами
    myHub.server.GetCountries();
    //загрузить чекбоксы с жанрами
    myHub.server.GetGenres();
    //загрузить рекомендации
    myHub.server.GetRecommendations();
}

//добавление чекбоксов со странами
function addCountry(country) {
    var newLabel = document.createElement("label");
    newLabel.innerHTML = "<label><input type=\"checkbox\">" + country + "</label>";
    document.getElementById("search-params-countries").appendChild(newLabel);
}

function addGenre(genre) {
    var newLabel = document.createElement("label");
    newLabel.innerHTML = "<label><input type=\"checkbox\">" + genre + "</label>";
    document.getElementById("search-params-genres").appendChild(newLabel);
}

function addRecommendations(films) {
    if (films.length > 0)
        for (var i = 0; i < films.length; i++) {
            var recommend = document.createElement("li");
            recommend.innerHTML = "<li>" + films[i] + "</li>";
            document.getElementById("recommendationList").appendChild(recommend);
        }
    else {
        var noRes = document.createElement("li");
        noRes.innerHTML = "<li>" + "В данный момент нет рекомендуемых фильмов" + "</li>";
        document.getElementById("recommendationList").appendChild(noRes);
    }
}