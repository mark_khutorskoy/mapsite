//var searchHub;

//function InitializeSearchAction() {
//    searchHub = $.connection.searchHub;
//    searchHub.client.addFilmToPage = addFilmToPage;
//    $.connection.hub.start().done(searchHubStarted);
//}

//function searchHubStarted() {
//    $('#button-search').click(Search);
//}

function Search() {
    var countries = [];
    var genres = [];

    $('#search-params-countries label').each(function() {
        if ($(this).children().is(':checked'))
            countries.push($(this).text());
    });

    $('#search-params-genres label').each(function() {
        if ($(this).children().is(':checked'))
            genres.push($(this).text());
    });

    var year1 = $('#selectYears1').val();
    var year2 = $('#selectYears2').val();
    var rating1 = $('#selectRating1').val();
    var rating2 = $('#selectRating2').val();

    //очистка и скрытие таблицы с результатами
    $("#search-results-table").empty(); 
    $("#search-results").show();

    myHub.server.Search(countries, genres, year1, year2, rating1, rating2);
}

//скрытие таблицы с результатами
$('#search-results').ready(function() {
    $("#search-results").hide();
})

function addFilmToPage(name, year, poster, genres, country, director, description, rating) {
    if (arguments.length == 1) {
        var noResults = document.createElement("p");
        noResults.innerHTML = "<p style=\"margin-left: 10px;\">Поиск не дал результатов</p>";
        document.getElementById("search-results-table").appendChild(noResults);
    } else {
        //формирование строки с жанрами
        var genresString = "";
        genresString += genres[0].charAt(0).toUpperCase() + genres[0].substring(1);
        for (var i = 1; i < genres.length; i++)
            genresString += ", "+ genres[i].charAt(0).toLowerCase() + genres[i].substring(1);

        var film = document.createElement("div");
        film.innerHTML =
            "<div class=\"search-result\">" +
                "<div class=\"result-film-poster\">" +
                    "<img src=\"images\\" + poster + "\" alt=\"Постер\">"+
                "</div>" +
                "<div class=\"result-film-info\">" +
                    "<h2>" + name + " ("+year+")</h2>" +
                    "<p>" + country + "</p>" +
                    "<p>" + genresString + "</p>"+
                    "<p>Режиссер: " + director + "</p>" +
                    "<p>" + description + "</p>" +
                "</div >"+
                "<div class=\"result-film-rating\">"+
                    "<p>"+
                        "<b>Рейтинг: " + rating + "/10</b>"+
                    "</p>"+
                "</div>" +
            "</div >" +
            "<hr>";
        document.getElementById("search-results-table").appendChild(film);
    }
}