﻿function getcontent() {
    var svgobject = document.getElementById('map');
    if ('contentDocument' in svgobject)
        var svgdom = svgobject.contentDocument;

    //проверка что выбрана страна
    if ($(svgdom.getElementsByClassName("area active")).attr('id')!=null &&
        $('.radiobtns:checked').attr('id')!=null) {
        
        myHub.server.getcontent(
            $('.radiobtns:checked').attr('id'),
            $(svgdom.getElementsByClassName("area active")).attr('id')
            );
    }
}

function content(moviename, description, year, directorfullname, country,rating,genre,trailer) {
    var pathPoster1 = "images/" + moviename + ".png";
    $('#poster').attr('src', pathPoster1);

    var pathPoster2 = "images/альт/" + moviename + ".png";
    $('#poster2').attr('src', pathPoster2);

    var pathMusic = "audio/" + moviename + ".mp3";
    $('#music').attr('src', pathMusic);
    if (document.getElementById("fig").style.display == "none") {
        var myAud = document.getElementById("music");
        myAud.pause();
    }

    var newdesc = document.getElementById('filmdescription');
    newdesc.innerText = description;

    var newyear = document.getElementById('movieyear');
    newyear.innerText = "Год:" + year;

    var newdirector = document.getElementById('moviedirector');
    newdirector.innerText = "Режиссер: " + directorfullname;

    var newcountry = document.getElementById('moviecountry');
    newcountry.innerText = "Страна: " + country;

    var newrating = document.getElementById('moviemark');
    newrating.innerText = "Оценка:  " + rating + "/10";

    var newtitle = document.getElementById('idTitle');
    newtitle.innerText = moviename;

    var newlink = document.getElementById('movielabel');
    newlink.innerText = moviename;

    var newgenre = document.getElementById('moviegenre');
    newgenre.innerText = "Жанр: " + genre;

    setvideo(trailer);
  
}