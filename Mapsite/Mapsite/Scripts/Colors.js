﻿$(window).load(function () {
    var svgobject = document.getElementById('map');
    if ('contentDocument' in svgobject)
        var svgdom = svgobject.contentDocument;

    LoadColor(RGBtoHEX($('body').css('background-color')), RGBtoHEX($('.color-inside').css('background-color')),
        RGBtoHEX($('.color-inside-2').css('background-color')), RGBtoHEX($('.area', svgdom).css('fill')), ($('.active', svgdom).css('fill') != undefined) ? RGBtoHEX($('.active', svgdom).css('fill')) : '#ffff00');
})

function LoadColor(background, color1, color2, colorMap2, colorMapActive) {
    $('#colorBackground').attr('value', background);
    $('#color1').attr('value', color1);
    $('#color2').attr('value', color2);
    $('#colorMap2').attr('value', colorMap2);
    $('#colorMapActive').attr('value', colorMapActive);
}

$(document).ready(function () {

jQuery('#template1').on('click', function () {

    $('body').css('background-color', '#fca4c1');
    $('.color-inside').css('background-color', ' rgb(161, 216, 177)');
    $('.color-inside-2').css('background-color', '#D9D853');
    $('button').css('background', '#D9D853');
    //кнопка главная, именно после button!
    $('.top-menu button.active').css('background', '#a1d8b1');
    
    //здесь недоступных цвет
    var svgobject = document.getElementById('map');
    if ('contentDocument' in svgobject)
        var svgdom = svgobject.contentDocument;
    $(svgdom.getElementsByTagName("polygon")).attr('fill', '#D9D853');
    $(svgdom.getElementsByTagName("polyline")).attr('fill', '#D9D853');

    //здесь достпуных цвет  area
    //$(svgdom.getElementsByClassName("area")).css('fill', 'red');
    var style = '.area {fill: #ff0000}';
    var str = $('.area', svgdom).css('fill');
    var st = $('svg > style', svgdom).text().replace('.area {fill: '+RGBtoHEX(str)+'}', "");
    $('svg > style', svgdom).text(st);
    $('svg > style', svgdom).prepend(style);

    style = '.active {fill: #ff12dd}';
    str = $('.active', svgdom).css('fill');
    if (str != undefined) var st = $('svg > style', svgdom).text().replace('.active {fill: ' + RGBtoHEX(str) + '}', "");
    else var st = $('svg > style', svgdom).text().replace('.active {fill: #ffff00}', "");
    $('svg > style', svgdom).text(st);
    $('svg > style', svgdom).append(style);

    LoadColor('#fca4c1', '#a1d8b1', '#d9d853', '#ff0000', '#ff12dd');
});

jQuery('#template0').on('click', function  () {
    $('body').css('background-color', '#fce5bf');
    $('.color-inside').css('background-color', ' #8BE5D3');

    $('.color-inside-2').css('background-color', '#C7D3F9');
    $('button').css('background', '#C7D3F9');
    //кнопка главная, именно после button!
    $('.top-menu button.active').css('background', '#8BE5D3');

    //здесь недоступных цвет
    var svgobject = document.getElementById('map');
    if ('contentDocument' in svgobject)
        var svgdom = svgobject.contentDocument;
    $(svgdom.getElementsByTagName("polygon")).attr('fill', '#C7D3F9');
    $(svgdom.getElementsByTagName("polyline")).attr('fill', '#C7D3F9');

    //здесь достпуных цвет  area
    var style = '.area {fill: #efffef}';
    var str = $('.area', svgdom).css('fill');
    var st = $('svg > style', svgdom).text().replace('.area {fill: ' + RGBtoHEX(str) + '}', "");
    $('svg > style', svgdom).text(st);
    $('svg > style', svgdom).prepend(style);

    style = '.active {fill: #ffff00}';
    str = $('.active', svgdom).css('fill');
    if (str != undefined) var st = $('svg > style', svgdom).text().replace('.active {fill: ' + RGBtoHEX(str) + '}', "");
    else var st = $('svg > style', svgdom).text().replace('.active {fill: #ffff00}', "");
    $('svg > style', svgdom).text(st);
    $('svg > style', svgdom).append(style);
  
    LoadColor('#fce5bf', '#8be5d3', '#c7d3f9', '#efffef', '#ffff00');
});

jQuery('#template2').on('click', function () {
    $('body').css('background-color', '#fff');
    $('.color-inside').css('background-color', '#E8747D');
    $('.color-inside-2').css('background-color', '#aadbff');
    $('button').css('background', '#aadbff');
    //кнопка главная, именно после button!
    $('.top-menu button.active').css('background', '#E8747D');

    //здесь недоступных цвет
    var svgobject = document.getElementById('map');
    if ('contentDocument' in svgobject)
        var svgdom = svgobject.contentDocument;
    $(svgdom.getElementsByTagName("polygon")).attr('fill', '#aadbff');
    $(svgdom.getElementsByTagName("polyline")).attr('fill', '#aadbff');

    //здесь достпуных цвет  area
    var style = '.area {fill: #00ff00}';
    var str = $('.area', svgdom).css('fill');
    var st = $('svg > style', svgdom).text().replace('.area {fill: ' + RGBtoHEX(str) + '}', "");
    $('svg > style', svgdom).text(st);
    $('svg > style', svgdom).prepend(style);

    style = '.active {fill: #1245cc}';
    str = $('.active', svgdom).css('fill');
    if (str != undefined) var st = $('svg > style', svgdom).text().replace('.active {fill: ' + RGBtoHEX(str) + '}', "");
    else var st = $('svg > style', svgdom).text().replace('.active {fill: #ffff00}', "");
    $('svg > style', svgdom).text(st);
    $('svg > style', svgdom).append(style);

    LoadColor('#ffffff', '#e8747d', '#aadbff', '#00ff00', '#1245cc');
});


//Пользовательский колорпикер
jQuery('#colorBackground').on('change', function () {
    $('body').css('background-color', $('#colorBackground').val());
});

jQuery('#color1').on('change', function () {
    $('.color-inside').css('background-color', $('#color1').val());
    $('.top-menu button.active').css('background', $('#color1').val());
});

jQuery('#color2').on('change', function () {
    $('.color-inside-2').css('background-color', $('#color2').val());
    $('button').css('background', $('#color2').val());
    $('.top-menu button.active').css('background', $('#color1').val()); //костыль

    //здесь недоступных цвет
    var svgobject = document.getElementById('map');
    if ('contentDocument' in svgobject)
        var svgdom = svgobject.contentDocument;
    $(svgdom.getElementsByTagName("polygon")).attr('fill', $('#color2').val());
    $(svgdom.getElementsByTagName("polyline")).attr('fill', $('#color2').val());
    
});

jQuery('#colorMap2').on('change', function () {
    
    //здесь достпуных цвет  area
    var svgobject = document.getElementById('map');
    if ('contentDocument' in svgobject)
        var svgdom = svgobject.contentDocument;

    var style = '.area {fill: ' + $('#colorMap2').val() + '}';
    var str = $('.area', svgdom).css('fill');
    var st = $('svg > style', svgdom).text().replace('.area {fill: ' + RGBtoHEX(str) + '}', "");
    $('svg > style', svgdom).text(st);
    $('svg > style', svgdom).prepend(style);
});

jQuery('#colorMapActive').on('change', function () {
    //здесь активной цвет
    //здесь достпуных цвет  area
    var svgobject = document.getElementById('map');
    if ('contentDocument' in svgobject)
        var svgdom = svgobject.contentDocument;

    var style = '.active {fill: ' + $('#colorMapActive').val() + '}';
    var str = $('.active', svgdom).css('fill');
    if (str != undefined) var st = $('svg > style', svgdom).text().replace('.active {fill: ' + RGBtoHEX(str) + '}', "");
    else var st = $('svg > style', svgdom).text().replace('.active {fill: #ffff00}', "");

    $('svg > style', svgdom).text(st);
    $('svg > style', svgdom).append(style);

});



})

//Function to convert rgb color to hex format
function RGBtoHEX(orig) {
    var rgb = orig.replace(/\s/g, '').match(/^rgba?\((\d+),(\d+),(\d+)/i);
    return (rgb && rgb.length === 4) ? "#" +
     ("0" + parseInt(rgb[1], 10).toString(16)).slice(-2) +
     ("0" + parseInt(rgb[2], 10).toString(16)).slice(-2) +
     ("0" + parseInt(rgb[3], 10).toString(16)).slice(-2) : orig;
}