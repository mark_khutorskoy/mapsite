// ��������� ���� ������� ����������/�������� ������, ��� ��� ��, ��� � jQuery �� �������� ��� SVG
jQuery.fn.myAddClass = function (classTitle) {
    return this.each(function () {
        var oldClass = jQuery(this).attr("class");
        oldClass = oldClass ? oldClass : '';
        jQuery(this).attr("class", (oldClass + " " + classTitle).trim());
    });
}
jQuery.fn.myRemoveClass = function (classTitle) {
    return this.each(function () {
        var oldClass = jQuery(this).attr("class");
        var startpos = oldClass.indexOf(classTitle);
        var endpos = startpos + classTitle.length;
        var newClass = oldClass.substring(0, startpos).trim() + " " + oldClass.substring(endpos).trim();
        if (newClass.trim()) jQuery(this).attr("class", newClass.trim());
    });
}

// �������� ������ ����� �������� ��������� ��������� (������� �������)
$(window).load(function () {
    // �������� ������ � SVG DOM
    var svgobject = document.getElementById('map');
    if ('contentDocument' in svgobject)
        var svgdom = svgobject.contentDocument;

    $(svgdom.getElementsByClassName("area")).click(
    function () {
        var id = $(this).attr("id");
        $(".area", svgdom).myRemoveClass("active");
        $("#" + id, svgdom).myAddClass("active");
    })

}
)

//function test() {
//    $('#main').css('fill', 'yellow');
//}
