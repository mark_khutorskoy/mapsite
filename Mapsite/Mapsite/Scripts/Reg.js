﻿function Reg() {
    myHub.server.Reg($('#tbLogin').val(), $('#tbPassword').val(), $('#tbRepeatPassword').val());
}

function Log() {
    myHub.server.Log($('#tbLoginLog').val(), $('#tbPasswordLog').val());
}

function CloseReg()
{
    $('#id02').hide();
}

function Logout()
{
    myHub.server.Logout();
    $('#lblUser').text('');
    $('#btnLoginIndex').show();
    $('#btnLogout').hide();
    $('#btnUsersControl').hide();
    $('#ChangeColors').hide();
    
    //меняем цвета на исходные
    $('body').css('background-color', '#fce5bf');
    $('.color-inside').css('background-color', ' #8BE5D3');

    $('.color-inside-2').css('background-color', '#C7D3F9');
    $('button').css('background', '#C7D3F9');
    //кнопка главная, именно после button!
    $('.top-menu button.active').css('background', '#8BE5D3');

    //здесь недоступных цвет
    var svgobject = document.getElementById('map');
    if ('contentDocument' in svgobject)
        var svgdom = svgobject.contentDocument;
    $(svgdom.getElementsByTagName("polygon")).attr('fill', '#C7D3F9');
    $(svgdom.getElementsByTagName("polyline")).attr('fill', '#C7D3F9');

    //здесь достпуных цвет  area
    var style = '.area {fill: #efffef}';
    var str = $('.area', svgdom).css('fill');
    var st = $('svg > style', svgdom).text().replace('.area {fill: ' + RGBtoHEX(str) + '}', "");
    $('svg > style', svgdom).text(st);
    $('svg > style', svgdom).prepend(style);

    style = '.active {fill: #ffff00}';
    str = $('.active', svgdom).css('fill');
    if (str != undefined) var st = $('svg > style', svgdom).text().replace('.active {fill: ' + RGBtoHEX(str) + '}', "");
    else var st = $('svg > style', svgdom).text().replace('.active {fill: #ffff00}', "");
    $('svg > style', svgdom).text(st);
    $('svg > style', svgdom).append(style);

    LoadColor('#fce5bf', '#8be5d3', '#c7d3f9', '#efffef', '#ffff00');
}

function LoginChange(user, role,status) {
    $('#btnLoginIndex').hide();
    $('#btnLogout').show();
    $('#lblUser').text(user);
    $('#id01').hide();
    $('#ChangeColors').show();
    if (role == 2) $('#btnUsersControl').show(); //для админа открываем контролюзеров
    if (status == 1) $('#banform').show(); //если забанен
    myHub.server.LoadColors(user);
}

function ErrorLoginLog()
{
    $('#lblErrorLoginLog').text('Неверный логин');
}

function ErrorPasswordLog() {
    $('#lblErrorPasswordLog').text('Неверный пароль');
}

function NotErrorPasswordLog() {
    $('#lblErrorPasswordLog').text('');
}

function NotErrorLoginLog() {
    $('#lblErrorLoginLog').text('');
}

function ErrorLoginReg(mess) {
    $('#lblErrorLoginReg').text(mess);
}

function ErrorPasswordReg(mess) {
    $('#lblErrorPasswordReg').text(mess);
}

function NotErrorPasswordReg() {
    $('#lblErrorPasswordReg').text('');
}

function NotErrorLoginReg() {
    $('#lblErrorLoginReg').text('');
}

function CheckLogin()
{
    myHub.server.CheckLogin($('#tbLogin').val());
}

function CheckPassword()
{
    if ($('#tbPassword').val()!=$('#tbRepeatPassword').val())
    {
        ErrorPasswordReg('Пароли не совпадают');
    }
    else
    {
        NotErrorPasswordReg();
    }
}
