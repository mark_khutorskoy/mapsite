﻿function getusers()
{
    myHub.server.getusers();
}

function usersHTML(users)
{
    $("table.userstable").html(users);
}

function getCheckBoxes() {

    var str = "";
    $('.ids').each(function (i, elem) {
        if ($(this).prop('checked')) str += $(elem).val()+'/';
    });

    return str;
}

function banUsers()
{
    myHub.server.banUsers(getCheckBoxes());
}

function UnbanUsers() {
    myHub.server.UnbanUsers(getCheckBoxes());
}

function deleteUsers()
{
    myHub.server.deleteUsers(getCheckBoxes());
}
