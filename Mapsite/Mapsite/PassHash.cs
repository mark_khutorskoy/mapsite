﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Mapsite
{
    public class PassHash
    {
        public static string CreateSalt(int size)
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buff = new byte[size];
            rng.GetBytes(buff);
            return Convert.ToBase64String(buff);
        }

        public static string CreateHash(string saltpassword)
        {
            using (var cr = new MD5CryptoServiceProvider())
            {
                return Convert.ToBase64String(cr.ComputeHash(Encoding.UTF8.GetBytes(saltpassword)));
            }
        }

        public static bool CheckHash(string hash, string saltpassword)
        {
            return hash == CreateHash(saltpassword);
        }
    }
}