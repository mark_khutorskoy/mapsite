﻿//using System;
//using System.Collections.Generic;
//using System.Web.UI.WebControls;
//using MathNet.Numerics.LinearAlgebra;
//using MathNet.Numerics.LinearAlgebra.Double;
//using Microsoft.AspNet.SignalR;
//using Microsoft.AspNet.SignalR.Hubs;
//using ORM;

//namespace Mapsite
//{
//    [HubName("searchHub")]
//    public class SearchHub : Hub
//    {
//        //поиск в соответствии с выбранными параметрами
//        [HubMethodName("Search")]
//        public void Search(List<string> countries, List<string> genres, int year1, int year2, int rating1, int rating2)
//        {
//            List<Film> result = new List<Film>();

//            using (var ctx = new dbFilmMapEntities())
//            {
//                foreach (var film in ctx.films.SortBy("rating"))
//                {
//                    bool flag = false;
//                    //сравнение по году и рейтингу
//                    if (film.year >= year1 && film.year <= year2 && film.rating >= rating1 && film.rating <= rating2)
//                    {
//                        if (genres.Count > 0)
//                        {
//                            foreach (var genre in film.genres)
//                                if (genres.Contains(genre.name))
//                                    flag = true;
//                        }
//                        //если не были выбраны жанры, они не учитываются при поиске
//                        else
//                            flag = true;
//                    }

//                    if (flag)
//                    {
//                        bool flag1 = false;
//                        if (countries.Count > 0)
//                        {
//                            if (countries.Contains(ctx.countries.Find(film.country).name))
//                                flag1 = true;
//                        }
//                        else
//                            flag1 = true;

//                        if (flag1)
//                        {
//                            List<string> genresList = new List<string>();
//                            foreach (var g in film.genres)
//                                genresList.Add(g.name);

//                            Film tFilm = new Film
//                            {
//                                Name = film.name,
//                                Year = film.year,
//                                Poster = $"{film.name}.png",
//                                Genres = genresList,
//                                Country = ctx.countries.Find(film.country).name,
//                                Director = $"{ctx.directors.Find(film.director).first_name} {ctx.directors.Find(film.director).second_name}",
//                                Description = film.description,
//                                Rating = film.rating
//                            };
//                            result.Add(tFilm);
//                        }
//                    }
//                }
//            }
//            result.Reverse();
//            if (result.Count > 0)
//                foreach (var item in result)
//                    Clients.Caller.addFilmToPage(item.Name, item.Year, item.Poster, item.Genres, item.Country, item.Director, item.Description, item.Rating);
//            else
//                Clients.Caller.addFilmToPage("По вашему запросу ничего не найдено");
//        }

//        //получение стран из БД
//        [HubMethodName("GetCountries")]
//        public void GetCountries()
//        {
//            using (var ctx = new dbFilmMapEntities())
//                foreach (var country in ctx.countries.SortBy("name"))
//                    Clients.Caller.addCountry(country.name);
//        }

//        //получение жанров из БД
//        [HubMethodName("GetGenres")]
//        public void GetGenres()
//        {
//            using (var ctx = new dbFilmMapEntities())
//                foreach (var genre in ctx.genres.SortBy("name"))
//                    Clients.Caller.addGenre(genre.name);
//        }

//        //получение рекомендаций
//        [HubMethodName("GetRecommendations")]
//        public void GetRecommendations()
//        {
//            //ID текущего пользователя
//            int currentId = 16;

//            //оценки текущего пользователя
//            List<film_marks> currentUserMarks = new List<film_marks>();
//            //оценки других пользователей
//            List<film_marks> otherUsersMarks = new List<film_marks>();
//            //ID других пользователей, которые ставили оценки (без ID текущего)
//            List<int> usersId = new List<int>();
//            //список рекомендуемых фильмов
//            List<string> result = new List<string>();

//            using (var ctx = new dbFilmMapEntities())
//            {
//                //считывание всех оценок
//                foreach (var mark in ctx.film_marks.SortBy("iduser"))
//                {
//                    if (mark.iduser == currentId)
//                        currentUserMarks.Add(mark);
//                    else
//                        otherUsersMarks.Add(mark);
//                    if (mark.iduser != currentId && !usersId.Contains(mark.iduser))
//                        usersId.Add(mark.iduser);
//                }

//                //рабочая копия оценок других пользователей

//                //фильмы, которые не оценил текущий пользователь
//                List<film_marks> notCommonFilms = new List<film_marks>();

//                int n = 10; //максимальное количество фильмов, которые должны рекомендоваться
//                double thresholdCorr = 0.8; //порог коэффициента корреляции Пирсона, по которому выбираются пользователи, у которых берутся фильмы
//                int thresholdMark = 7; //минимальная оценка, с которой рекомендуются фильмы
//                int thresholdCommonFilmsCount = 3; //минимальное количество общих фильмов

//                //последняя рассмотренная оценка
//                int last = 0;
//                //проходим по другим пользователям
//                for (int i = 0; i < usersId.Count && result.Count < n; i++)
//                {
//                    List<film_marks> commonFilms1 = new List<film_marks>(); //общие фильмы с оценками текущего пользователя
//                    List<film_marks> commonFilms2 = new List<film_marks>(); //общие фильмы с оценками рассматриваемого пользователя

//                    //проход по оцененным фильмам рассматриваемого пользователя
//                    for (int j = last; j < otherUsersMarks.Count && usersId[i] == otherUsersMarks[j].iduser; j++, last++)
//                    {
//                        bool found = false;
//                        //проход по оцененным фильмам текущего пользователя
//                        for (int k = 0; k < currentUserMarks.Count; k++)
//                        {
//                            //если нашелся общий фильм
//                            if (currentUserMarks[k].idfilm == otherUsersMarks[j].idfilm)
//                            {
//                                //добавление общего фильма в списки для каждого пользователя
//                                commonFilms1.Add(currentUserMarks[k]);
//                                commonFilms2.Add(otherUsersMarks[j]);

//                                found = true;
//                                break;
//                            }
//                        }
//                        //если фильм не является общим
//                        if (!found)
//                            notCommonFilms.Add(otherUsersMarks[j]);
//                    }

//                    //если общее количество фильмов больше или равно порога
//                    if (commonFilms1.Count >= thresholdCommonFilmsCount)
//                    {
//                        Vector<double> curMarks = new DenseVector(commonFilms1.Count); //оценки текущего пользователя
//                        Vector<double> otherMarks = new DenseVector(commonFilms2.Count); //оценки рассматриваемого пользователя

//                        //оценки текущего и рассматриваемого пользователей
//                        //for (int j = 0; j < commonFilms1.Count; j++)
//                        //{
//                        //    curMarks[j] = commonFilms1[j].user_mark;
//                        //    otherMarks[j] = commonFilms2[j].user_mark;
//                        //}

//                        //коэффициент корреляции Пирсона между оценками текущего и рассматриваемого пользователей
//                        double corr = Math.Abs(MathNet.Numerics.Statistics.Correlation.Pearson(curMarks, otherMarks));

//                        //если коэффициент проходит по порогу
//                        if (corr >= thresholdCorr)
//                            for (int j = 0; j < notCommonFilms.Count && result.Count < n; j++)
//                            {
//                                //если фильм проходит по порогу с оценкой
//                                if (notCommonFilms[j].user_mark >= thresholdMark)
//                                {
//                                    string film = ctx.films.Find(notCommonFilms[j].idfilm).name;
//                                    //если в результате нет не общего фильма
//                                    if (!result.Contains(film))
//                                        result.Add(film);
//                                }
//                            }
//                    }
//                }
//            }

//            Clients.Caller.addRecommendations(result);
//        }
//    }

//    public class Film
//    {
//        public string Name;
//        public int Year;
//        public string Poster;
//        public string Country;
//        public List<string> Genres;
//        public string Director;
//        public string Description;
//        public double? Rating;

//        public Film()
//        {
//            Genres = new List<string>();
//        }
//    }
//}