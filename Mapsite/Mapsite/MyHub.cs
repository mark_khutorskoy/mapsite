﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using ORM;
using System.Threading.Tasks;

using System.Web.UI.WebControls;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;


namespace Mapsite
{
    [HubName("myHub")]
    public class MyHub : Hub
    {
        string cid
        {
            get
            {
                string clientId = "";
                if (Context.QueryString["clientId"] != null)
                {
                    clientId = Context.QueryString["clientId"];
                }
                if (string.IsNullOrWhiteSpace(clientId))
                {
                    clientId = Context.ConnectionId;
                }
                return clientId;
            }
        }

        [HubMethodName("Log")]
        public void LogIn(string user, string pass)
        {
            users U = null;
            using (var ctx = new dbFilmMapEntities())
            {
                foreach (var us in ctx.users)
                {
                    if (us.login == user)
                    {
                        U = us;
                        break;
                    }
                }
            }
            if (U == null) //не нашли юзера
            {
                Clients.Caller.ErrorLoginLog();
                Clients.Caller.NotErrorPasswordLog();
            }
            else
            {
                if (!PassHash.CheckHash(U.password, pass + U.salt)) //неправильный пароль
                {
                    Clients.Caller.ErrorPasswordLog();
                    Clients.Caller.NotErrorLoginLog();
                }
                else
                {
                    Users.ConnectUser(new WebUser(U, cid));
                    Clients.Caller.LoginChange(U.login, U.idRole, U.idStatus);
                    Clients.Caller.NotErrorLoginLog();
                    Clients.Caller.NotErrorPasswordLog();
                }
            }
        }

        [HubMethodName("Reg")]
        public void Reg(string login, string pass, string repeatpass)
        {
            if (pass == "") Clients.Caller.ErrorPasswordReg("Пароль не может быть пустым");
            if (login == "") Clients.Caller.ErrorLoginReg("Логин не может быть пустым");
            if (CheckLogin(login) && pass == repeatpass && pass != "" && login != "")
            {
                var rnd = new Random();
                var salt = PassHash.CreateSalt(rnd.Next(20, 30));
                var user = new users()
                {
                    login = login,
                    password = PassHash.CreateHash(pass + salt),
                    salt = salt,
                    idRole = 1

                };
                using (var ctx = new dbFilmMapEntities())
                {
                    ctx.users.Add(user);
                    ctx.SaveChanges();
                }
                Clients.Caller.CloseReg();
            }
        }

        [HubMethodName("CheckLogin")]
        public bool CheckLogin(string login)
        {
            users U = null;
            using (var ctx = new dbFilmMapEntities())
            {
                foreach (var us in ctx.users)
                {
                    if (us.login == login)
                    {
                        U = us;
                        break;
                    }
                }
            }
            if (U != null)
            {
                Clients.Caller.ErrorLoginReg("Логин уже занят");
            }
            else
            {
                if (login != "")
                {
                    Clients.Caller.NotErrorLoginReg();
                }
            }
            return U == null;
        }

        [HubMethodName("Logout")]
        public void Logout()
        {
            Users.DisconnectUser(Users.UserByCid(cid));
        }

        [HubMethodName("Session")]
        public void Session()
        {
            var u = Users.UserByCid(cid);
            if (u != null)
            {
                Clients.Caller.LoginChange(u._User.login);
            }
        }

        [HubMethodName("getcontent")]
        public void getcontent(string year, string country)
        {
            int yearInt = Convert.ToInt32(year);
            string moviename = "null";

            //идем по мини-таблице и заполняем словать страны - id
            Dictionary<string, int> countryname = new Dictionary<string, int>();
            using (var a = new dbFilmMapEntities())
            {
                foreach (var b in a.countries)
                {
                    countryname.Add(b.name, b.id);
                }
            }
            //получили из словаря id страны и заполнили
            int countryId = countryname[country];

            //идем по режиссерам
            Dictionary<int, string> directorsname = new Dictionary<int, string>();
            string[] FLname = new string[2];
            string fullname;
            using (var a = new dbFilmMapEntities())
            {
                foreach (var b in a.directors)
                {
                    FLname[0] = b.first_name;
                    FLname[1] = b.second_name;
                    fullname = FLname[0] + " " + FLname[1];
                    directorsname.Add(b.id, fullname);
                }
            }


            string description = "none";
            int exactyear = 0;
            string directorfullname = "none";
            double rating = 0;
            int filmId = 0;
            //string genre = "";
            string totalgenre = "";
            int counter = 0;
            string trailer = "";

            //идем по таблице films и возвращаем название страны
            using (var ctx = new dbFilmMapEntities())
            {
                foreach (var us in ctx.films)
                {
                    //ищем название фильма в таблице films
                    if ((Convert.ToInt32(Math.Ceiling(us.year / 5.0) * 5) == yearInt || us.year == yearInt) && us.country == countryId)
                    {
                        moviename = us.name;
                        description = us.description;
                        exactyear = us.year;
                        directorfullname = directorsname[us.director];
                        rating = Convert.ToDouble(us.rating);
                        filmId = us.id;
                        trailer = us.trailer;

                        foreach (var genre in us.genres)
                        {
                            if (counter == 0)
                            {
                                totalgenre = genre.name;
                                counter++;
                            }
                            else totalgenre = totalgenre + ", " + genre.name;
                        }
                        counter = 0;

                        break;
                    }
                }
            }

            using (var ctx = new dbFilmMapEntities())
            {
                //foreach (var us in ctx.genres)
                //{
                //    if (us.id == yearInt )
                //}
            }
            //вызвали функцию JS
            Clients.Caller.content(moviename, description, exactyear, directorfullname, country, rating, totalgenre, trailer);


        }

        [HubMethodName("getusers")]
        public void getusers()
        {
            Dictionary<int, string> status = new Dictionary<int, string>();
            using (var a = new dbFilmMapEntities())
            {
                foreach (var b in a.statuses)
                {
                    status.Add(b.id, b.status);
                }
            }

            string htmlstring = "";
            int curId = 0;
            using (var a = new dbFilmMapEntities())
            {
                htmlstring = "<tr class='color-inside-2'> <th class='col1'></th> <th class='col2'>Login</th> <th class='col3'>Статус</th></tr>";
                curId++;
                string statusVal = "";
                foreach (var b in a.users)
                {
                    //не даем банить админов
                    //if(b.isAdmin!=true)
                    if (b.idRole != 2)
                    {
                        if (b.idStatus == null) statusVal = null;
                        else statusVal = status[(int)b.idStatus];

                        htmlstring = htmlstring + " <tr> <td><input id=" + curId + " class='ids' type='checkbox' value=" + b.id + "></td> <td>" + b.login + "</td> <td>" + statusVal + "</td> </tr>";
                    }
                }
            }
            Clients.Caller.usersHTML(htmlstring);
        }

        [HubMethodName("banUsers")]
        public void banUsers(string users)
        {


            string[] split = users.Split('/');
            int i = 0;
            using (var a = new dbFilmMapEntities())
            {
                foreach (var b in a.users)
                {
                    if (split[i] != "")
                    {
                        if (Convert.ToInt32(split[i]) == b.id)
                        {

                            // b.status = "Забанен";
                            b.idStatus = 1;
                            i++;
                        }
                    }
                }
                //сохраняем изменения в БД
                a.SaveChanges();
            }
            //обновляем табличку
            getusers();
        }

        [HubMethodName("UnbanUsers")]
        public void UnbanUsers(string users)
        {
            string[] split = users.Split('/');
            int i = 0;
            using (var a = new dbFilmMapEntities())
            {
                foreach (var b in a.users)
                {
                    if (split[i] != "")
                    {
                        if (Convert.ToInt32(split[i]) == b.id)
                        {
                            //b.status = "";
                            b.idStatus = null;
                            i++;
                        }
                    }
                }
                //сохраняем изменения в БД
                a.SaveChanges();
            }
            //обновляем табличку
            getusers();
        }


        [HubMethodName("deleteUsers")]
        public void deleteUsers(string users)
        {
            string[] split = users.Split('/');
            int i = 0;
            using (var a = new dbFilmMapEntities())
            {
                foreach (var b in a.users)
                {
                    if (split[i] != "")
                    {
                        if (Convert.ToInt32(split[i]) == b.id)
                        {
                            a.users.Remove(b);
                            i++;
                        }
                    }
                }
                a.SaveChanges();
            }
            //обновляем табличку
            getusers();
        }

        [HubMethodName("saveColor")]
        public void SaveColor(string background, string color1, string color2, string colorMap2, string colorMapActive, string login)
        {
            var colors = new ColorScheme();
            colors.background = background;
            colors.maincolor = color1;
            colors.additional = color2;
            colors.mapAdditional = colorMap2;
            colors.mapActive = colorMapActive;
            int idColor = 0;

            using (var ctx = new dbFilmMapEntities())
            {
                foreach (var user in ctx.users)
                {
                    if (user.login == login)
                    {
                        if (user.idColorScheme == null)
                        {
                            var newcolor = ctx.ColorScheme.Add(colors);
                            user.idColorScheme = newcolor.id;
                        }
                        else
                        {
                            idColor = (int)user.idColorScheme;
                            foreach (var color in ctx.ColorScheme)
                            {
                                if (color.id == idColor)
                                {
                                    color.background = background;
                                    color.maincolor = color1;
                                    color.additional = color2;
                                    color.mapAdditional = colorMap2;
                                    color.mapActive = colorMapActive;
                                    break;
                                }
                            }
                        }
                        break;
                    }
                }
                ctx.SaveChanges();
            }


        }

        [HubMethodName("LoadColors")]
        public void LoadColors(string login)
        {
            int? idColor = 0;
            using (var ctx = new dbFilmMapEntities())
            {
                foreach (var user in ctx.users)
                {
                    if (user.login == login)
                    {
                        idColor = user.idColorScheme;
                    }
                    if (idColor == null) return;
                }

                foreach (var color in ctx.ColorScheme)
                {
                    if (color.id == idColor)
                    {
                        Clients.Caller.fillColors(color.background.Replace(" ", ""), color.maincolor.Replace(" ", ""), color.additional.Replace(" ", ""), color.mapAdditional.Replace(" ", ""), color.mapActive.Replace(" ", ""));
                        break;
                    }
                }


            }
        }

        //поиск в соответствии с выбранными параметрами
        [HubMethodName("Search")]
        public void Search(List<string> countries, List<string> genres, int year1, int year2, int rating1, int rating2)
        {
            List<Film> result = new List<Film>();

            using (var ctx = new dbFilmMapEntities())
            {
                foreach (var film in ctx.films.SortBy("rating"))
                {
                    bool flag = false;
                    //сравнение по году и рейтингу
                    if (film.year >= year1 && film.year <= year2 && film.rating >= rating1 && film.rating <= rating2)
                    {
                        if (genres.Count > 0)
                        {
                            foreach (var genre in film.genres)
                                if (genres.Contains(genre.name))
                                    flag = true;
                        }
                        //если не были выбраны жанры, они не учитываются при поиске
                        else
                            flag = true;
                    }

                    if (flag)
                    {
                        bool flag1 = false;
                        if (countries.Count > 0)
                        {
                            if (countries.Contains(ctx.countries.Find(film.country).name))
                                flag1 = true;
                        }
                        else
                            flag1 = true;

                        if (flag1)
                        {
                            List<string> genresList = new List<string>();
                            foreach (var g in film.genres)
                                genresList.Add(g.name);

                            Film tFilm = new Film
                            {
                                Name = film.name,
                                Year = film.year,
                                Poster = $"{film.name}.png",
                                Genres = genresList,
                                Country = ctx.countries.Find(film.country).name,
                                Director = $"{ctx.directors.Find(film.director).first_name} {ctx.directors.Find(film.director).second_name}",
                                Description = film.description,
                                Rating = film.rating
                            };
                            result.Add(tFilm);
                        }
                    }
                }
            }
            result.Reverse();
            if (result.Count > 0)
                foreach (var item in result)
                    Clients.Caller.addFilmToPage(item.Name, item.Year, item.Poster, item.Genres, item.Country, item.Director, item.Description, item.Rating);
            else
                Clients.Caller.addFilmToPage("По вашему запросу ничего не найдено");
        }

        //получение стран из БД
        [HubMethodName("GetCountries")]
        public void GetCountries()
        {
            using (var ctx = new dbFilmMapEntities())
                foreach (var country in ctx.countries.SortBy("name"))
                    Clients.Caller.addCountry(country.name);
        }

        //получение жанров из БД
        [HubMethodName("GetGenres")]
        public void GetGenres()
        {
            using (var ctx = new dbFilmMapEntities())
                foreach (var genre in ctx.genres.SortBy("name"))
                    Clients.Caller.addGenre(genre.name);
        }

        //получение рекомендаций
        [HubMethodName("GetRecommendations")]
        public void GetRecommendations()
        {
            //ID текущего пользователя
            int currentId = 16;

            //оценки текущего пользователя
            List<film_marks> currentUserMarks = new List<film_marks>();
            //оценки других пользователей
            List<film_marks> otherUsersMarks = new List<film_marks>();
            //ID других пользователей, которые ставили оценки (без ID текущего)
            List<int> usersId = new List<int>();
            //список рекомендуемых фильмов
            List<string> result = new List<string>();

            using (var ctx = new dbFilmMapEntities())
            {
                //считывание всех оценок
                foreach (var mark in ctx.film_marks.SortBy("iduser"))
                {
                    if (mark.iduser == currentId)
                        currentUserMarks.Add(mark);
                    else
                        otherUsersMarks.Add(mark);
                    if (mark.iduser != currentId && !usersId.Contains(mark.iduser))
                        usersId.Add(mark.iduser);
                }

                //рабочая копия оценок других пользователей

                //фильмы, которые не оценил текущий пользователь
                List<film_marks> notCommonFilms = new List<film_marks>();

                int n = 10; //максимальное количество фильмов, которые должны рекомендоваться
                double thresholdCorr = 0.8; //порог коэффициента корреляции Пирсона, по которому выбираются пользователи, у которых берутся фильмы
                int thresholdMark = 7; //минимальная оценка, с которой рекомендуются фильмы
                int thresholdCommonFilmsCount = 3; //минимальное количество общих фильмов

                //последняя рассмотренная оценка
                int last = 0;
                //проходим по другим пользователям
                for (int i = 0; i < usersId.Count && result.Count < n; i++)
                {
                    List<film_marks> commonFilms1 = new List<film_marks>(); //общие фильмы с оценками текущего пользователя
                    List<film_marks> commonFilms2 = new List<film_marks>(); //общие фильмы с оценками рассматриваемого пользователя

                    //проход по оцененным фильмам рассматриваемого пользователя
                    for (int j = last; j < otherUsersMarks.Count && usersId[i] == otherUsersMarks[j].iduser; j++, last++)
                    {
                        bool found = false;
                        //проход по оцененным фильмам текущего пользователя
                        for (int k = 0; k < currentUserMarks.Count; k++)
                        {
                            //если нашелся общий фильм
                            if (currentUserMarks[k].idfilm == otherUsersMarks[j].idfilm)
                            {
                                //добавление общего фильма в списки для каждого пользователя
                                commonFilms1.Add(currentUserMarks[k]);
                                commonFilms2.Add(otherUsersMarks[j]);

                                found = true;
                                break;
                            }
                        }
                        //если фильм не является общим
                        if (!found)
                            notCommonFilms.Add(otherUsersMarks[j]);
                    }

                    //если общее количество фильмов больше или равно порога
                    if (commonFilms1.Count >= thresholdCommonFilmsCount)
                    {
                        Vector<double> curMarks = new DenseVector(commonFilms1.Count); //оценки текущего пользователя
                        Vector<double> otherMarks = new DenseVector(commonFilms2.Count); //оценки рассматриваемого пользователя

                        //оценки текущего и рассматриваемого пользователей
                        //for (int j = 0; j < commonFilms1.Count; j++)
                        //{
                        //    curMarks[j] = commonFilms1[j].user_mark;
                        //    otherMarks[j] = commonFilms2[j].user_mark;
                        //}

                        //коэффициент корреляции Пирсона между оценками текущего и рассматриваемого пользователей
                        double corr = Math.Abs(MathNet.Numerics.Statistics.Correlation.Pearson(curMarks, otherMarks));

                        //если коэффициент проходит по порогу
                        if (corr >= thresholdCorr)
                            for (int j = 0; j < notCommonFilms.Count && result.Count < n; j++)
                            {
                                //если фильм проходит по порогу с оценкой
                                if (notCommonFilms[j].user_mark >= thresholdMark)
                                {
                                    string film = ctx.films.Find(notCommonFilms[j].idfilm).name;
                                    //если в результате нет не общего фильма
                                    if (!result.Contains(film))
                                        result.Add(film);
                                }
                            }
                    }
                }
            }

            Clients.Caller.addRecommendations(result);
        }

    }

    public class Film
    {
        public string Name;
        public int Year;
        public string Poster;
        public string Country;
        public List<string> Genres;
        public string Director;
        public string Description;
        public double? Rating;

        public Film()
        {
            Genres = new List<string>();
        }
    }
}




    
