//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ORM
{
    using System;
    using System.Collections.Generic;
    
    public partial class ColorScheme
    {
        public ColorScheme()
        {
            this.users = new HashSet<users>();
        }
    
        public int id { get; set; }
        public string background { get; set; }
        public string maincolor { get; set; }
        public string additional { get; set; }
        public string mapAdditional { get; set; }
        public string mapActive { get; set; }
    
        public virtual ICollection<users> users { get; set; }
    }
}
